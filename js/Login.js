var mycv = angular.module('myshop', ['ngMaterial', 'firebase','ngMdIcons']);

mycv.controller('myCtrl', ['$scope', '$firebaseArray','$firebaseObject', function($scope, $firebaseArray,$firebaseObject){
   
   var ref = new Firebase('https://datashopping-alpha.firebaseio.com/');
	var authData1 = ref.getAuth();
    
    $scope.logup = function(){		
		$scope.profile = $firebaseArray(ref.child('user'));
		$scope.username = $.trim($scope.username);

		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; 
		if (!filter.test($scope.emaillogup)) 
			{ 
				alert('Hãy nhập lại địa chỉ email hợp lệ.\nExample@gmail.com');
				//$("#form_User").focus(); 
				return false; 
		}
		if ($scope.passlogup.length < 8) {
			alert('Hãy nhập lại mật khẩu dài hơn 8');
			//$("#form_Password").focus(); 
			return false;
		}
		if(!($scope.passlogup === $scope.repasslogup))		
		{
			alert('Hãy nhập lại mật khẩu trùng khớp');
			//$("#form_Password").focus(); 
			return false;
		}else{
		ref.createUser({
			  email    : $scope.emaillogup,
			  password : $scope.passlogup
			}, function(error, userData) {
			  if (error) {
			    console.log(error);
			    if (error.message.indexOf("email address is already in use") !== -1) {
			    	alert('Email đã được đăng kí.');
					//$("#form_User").focus(); 
					return false;
			    }
			  } else {
			    console.log("Successfully created user account with uid:", userData.uid);
			    
				alert('Đăng kí thành công.'); 
                
			  }
			});
	   }
	}
    
    $scope.login = function(){
		ref.authWithPassword({
			  email    : $scope.username_login,
			  password : $scope.password
			}, function(error, authData) {
			  if (error) {
			    console.log("Login Failed!", error);
			    alert('Đăng nhập không thành công. \nKiểm tra lại mật khẩu.'); 
			    //$("#form_password").focus(); 
			    return false;
			  } else {
			    console.log("Authenticated successfully with payload:", authData);
				window.location = "index.html#/home";
			  }
			});	
					
	}
    
     $scope.loginFacebook = function(){
		ref.authWithOAuthPopup("facebook", function(error, authData) {
          if (error) {
            console.log("Login Failed!", error);
              
          } else {
             // console.log("Authenticated successfully with payload:", authData);
            // the access token will allow us to make Open Graph API calls
            console.log(authData.facebook.accessToken);
              window.location = "index.html#/home";
          }
        }, {
          scope: "email,user_likes" // the permissions requested
        });
					
	}
    
    $scope.about= "Nguyễn Thanh Hiền"
    $scope.facebook = "https://www.facebook.com/thanhhien1395"
    $scope.email = "nthhien79@gmail.com"
    $scope.github = "https://gitlab.com/u/ThanhHien"
    $scope.phone = "01698032879"
    $scope.info = "Hi, my name is Nguyen Thanh Hien and you can call me Hien. I'm 21 years old and I am from Moc Hoa district, Long An province which is near the border between Vietnam and Combodia.I am study in University of Science and this is the third year in Information Technology.My hobby is music, reading and badminton..."
    $scope.edu = ["COLLEGE/UNIVERSITY", "2013 - 2017: University of Science in Software Engineering.",
                  "LANGUAGE",
                    "Vietnamese (Native)",
                    "English",
                  ]
    $scope.attri = ["Creativity", "Hardworking", "Teamwork", "Communication"];
}]);