var app = angular.module('myCakeStore', ['ngRoute', 'myCakeStore.home', 'myCakeStore.cake', 'ngMaterial','ngMdIcons']);

app.config(function($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: 'home.html',
        controller: 'homeCtrl'
      })
      .when('/cake/:cakeType/:cakeId', {
        templateUrl: 'cake.html',
        controller: 'cakeCtrl'
      })
      .otherwise({
        redirectTo: '/home'
      })
  });
