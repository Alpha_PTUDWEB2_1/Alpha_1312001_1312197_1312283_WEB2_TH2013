var cakeModule = angular.module('myCakeStore.cake', []);

cakeModule.controller('cakeCtrl',['$scope', '$routeParams',function ($scope, $routeParams){
    $scope.params = $routeParams;

    var appURL = "https://sanpham.firebaseio.com";
    var cakeType = $scope.params.cakeType;

    if (cakeType == "banh-kem")
        $scope.cakeType = "Bánh Kem";
    else if (cakeType == "banh-ngot")
        $scope.cakeType = "Bánh Ngọt";
    else if (cakeType == "banh-man")
        $scope.cakeType = "Bánh Mặn";
    else $scope.cakeType = "Unknow cake";

    var ref = new Firebase(appURL + "/" + cakeType + '/' + $scope.params.cakeId);

    ref.on("value", function (snapshot) {
        $scope.cake = snapshot.val();
        $scope.$digest();
    });
}]);