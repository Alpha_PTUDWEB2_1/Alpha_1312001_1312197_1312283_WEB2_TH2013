var homeModule = angular.module('myCakeStore.home', ['ngMaterial','ngMdIcons']);

homeModule.controller('homeCtrl', ['$scope','$timeout', function($scope, $timeout){
    $scope.banhkem = [];
    $scope.banhman = [];
    $scope.banhngot = [];


    var dataURL = "https://datashopping-alpha.firebaseio.com/";
var refBanhKem = new Firebase(dataURL + "banhkem");
    refBanhKem.on("value", function (snapshot) {
        $scope.banhkem = snapshot.val();
        $scope.$digest();
        console.log("Banh kem");
        console.log(snapshot.val());
    });

    var refBanhMan = new Firebase(dataURL + "banhman");
    refBanhMan.on("value", function (snapshot) {
        $scope.banhman = snapshot.val();
        $scope.$digest();
        console.log("Banh man");
        console.log(snapshot.val());
    });

    var refBanhNgot = new Firebase(dataURL + "banhngot");
    refBanhNgot.on("value", function (snapshot) {
        $scope.banhngot = snapshot.val();
        $scope.$digest();
        console.log("Banh ngot");
        console.log(snapshot.val());
    });

	$timeout(function(){
        $(document).ready(function () {
                $('#horizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true   // 100% fit in a container
                });
            });
    }, 1000);

    /*var bk = $firebaseArray(ref.child('banhkem'));
    $scope.banhkem = [];
    var arrbanhkem;
    arrbanhkem = bk.slice();
    bk.$loaded(function(){
        arrbanhkem = bk.slice();
        for(var i = 0; i < arrbanhkem.length; i++){
            $scope.banhkem[i] = {
                gia:arrbanhkem[i].gia,
                image:arrbanhkem[i].image,
                ten_banh:arrbanhkem[i].ten_banh,
                tinhchat:arrbanhkem[i].tinhchat,
                trang_html:arrbanhkem[i].trang_html

            }
        }
    })

    var bn = $firebaseArray(ref.child('banhngot'));
    $scope.banhngot = [];
    var arrbanhngot;
    arrbanhngot = bn.slice();
    bn.$loaded(function(){
        arrbanhngot = bn.slice();
        for(var i = 0; i < arrbanhngot.length; i++){
            $scope.banhngot[i] = {
                gia:arrbanhngot[i].gia,
                image:arrbanhngot[i].image,
                ten_banh:arrbanhngot[i].ten_banh,
                tinhchat:arrbanhngot[i].tinhchat,
                trang_html:arrbanhngot[i].trang_html

            }
        }
    })

    var bm = $firebaseArray(ref.child('banhman'));
    $scope.banhman = [];
    var arrbanhman;
    arrbanhman = bm.slice();
    bm.$loaded(function(){
        arrbanhman = bm.slice();
        for(var i = 0; i < arrbanhman.length; i++){
            $scope.banhman[i] = {
                gia:arrbanhman[i].gia,
                image:arrbanhman[i].image,
                ten_banh:arrbanhman[i].ten_banh,
                tinhchat:arrbanhman[i].tinhchat,
                trang_html:arrbanhman[i].trang_html

            }
        }
    })*/
	
   $scope.loaibanh=[
        {
            control:'tab_item-0',
            loai:'Bánh Kem'
        },
        {
            control:'tab_item-1',
            loai:'Bánh Ngọt'
        },
       {
            control:'tab_item-2',
            loai:'Bánh Mặn'
        }
    ];
    
    
     $scope.loaibanh=[
        {
            control:'tab_item-0',
            loai:'Bánh Kem'
        },
        {
            control:'tab_item-1',
            loai:'Bánh Ngọt'
        },
        {
            control:'tab_item-2',
            loai:'Bánh Mặn'
        }
    ];
    
    $scope.tabthongtin=[
        {
            link:'index.html',
            tab:'Trang chủ'
        },
        {
            link:'mens.html',
            tab:'Bánh Kem'
        },
        {
            link:'womens.html',
            tab:'Cup Cake'
        },
        {
            link:'electronics.html',
            tab:'Bánh Ngọt'
        },
        {
            link:'codes.html',
            tab:'Bánh Mặn'
        },
        {
            link:'contact.html',
            tab:'Liên Hệ'
        }
    ];
    
    
    $scope.tabthongtin=[
        {
            link:'index.html',
            tab:'Trang chủ'
        },
        {
            link:'mens.html',
            tab:'Bánh Kem'
        },
        {
            link:'womens.html',
            tab:'Cup Cake'
        },
        {
            link:'electronics.html',
            tab:'Bánh Ngọt'
        },
        {
            link:'codes.html',
            tab:'Bánh Mặn'
        },
        {
            link:'contact.html',
            tab:'Liên Hệ'
        }
    ];
  
}]);